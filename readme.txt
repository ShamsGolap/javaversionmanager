Hi!

This is a project in Java about a "version file manager".

The idea is to have a software that is able to write a new version in alpha/beta/release/etc.

It should have a pattern maybe like this:



Current version:
0.3b
Mispelling corrections.

*************************
Versions history:
0.3b
Mispelling corrections.
-----
0.3
Added content to some html files.
-----
0.2
Second skeleton version.
Added content to index.html
-----
0.1
First skeleton version.
-----



The current version is visible at the top of the file.
Then, a line of '*' will be a separator between the current version and the history.

In the future, there should be an implementation that can allow the user to change the pattern.
